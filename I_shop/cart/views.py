
from django.shortcuts import render, get_object_or_404, redirect
from .models import Order, Cart
from products.models import Product
from django.contrib import messages
# Create your views here.

def add_to_cart(request, slug):
    item = get_object_or_404(Product, slug=slug)
    if request.user.is_anonymous:
        messages.warning(request, "Вы не зарегистрированны!")
        return redirect("/accounts/login/")
    order_item, created = Cart.objects.get_or_create(
        item=item,
        user=request.user
    )
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order = order_qs[0]
        # проверить, есть ли товар в заказе
        if order.orderitems.filter(item__slug=item.slug).exists():
            order_item.quantity += 1
            order_item.save()
            messages.info(request, "Число товара измененно")
            return redirect("mainapp:cart-home")
        else:
            order.orderitems.add(order_item)
            messages.info(request, "Товар добавлен в корзину.")
            return redirect("mainapp:home")
    else:
        order = Order.objects.create(
            user=request.user)
        order.orderitems.add(order_item)
        messages.info(request, "Товар добавлен в корзину.")
        return redirect("mainapp:home")
    

def remove_from_cart(request, slug):
    item = get_object_or_404(Product, slug=slug)
    if request.user.is_anonymous:
        messages.warning(request, "Вы не зарегистрированны!")
        return redirect("/accounts/login/")
    cart_qs = Cart.objects.filter(user=request.user, item=item)
    if cart_qs.exists():
        cart = cart_qs[0]
        # Проверка численности корзины
        if cart.quantity > 1:
            cart.quantity -= 1
            cart.save()
        else:
            cart_qs.delete()
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # проверить, есть ли товар в заказе
        if order.orderitems.filter(item__slug=item.slug).exists():
            order_item = Cart.objects.filter(
                item=item,
                user=request.user,
            )[0]
            order.orderitems.remove(order_item)
            messages.info(request, "Товар убран из корзины.")
            return redirect("mainapp:home")
        else:
            messages.info(request, "Этого товара и так не было в корзине!")
            return redirect("mainapp:home")
    else:
        messages.info(request, "У вас не активного заказа")
        return redirect("core:home")


def CartView(request):

    user = request.user
    if user.is_anonymous:
        messages.warning(request, "Вы не зарегистрированны!")
        return redirect("/accounts/login/")
    else:
        carts = Cart.objects.filter(user=user)
        orders = Order.objects.filter(user=user, ordered=False)

        if carts.exists():
            order = orders[0]
            return render(request, 'cart/home.html', {"carts": carts, 'order': order})
            
        else:
            messages.warning(request, "У Вас нет активных заказов")
            return redirect("mainapp:home")


def decreaseCart(request, slug):
    item = get_object_or_404(Product, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # проверить, есть ли товар в заказе
        if order.orderitems.filter(item__slug=item.slug).exists():
            order_item = Cart.objects.filter(
                item=item,
                user=request.user
            )[0]
            if order_item.quantity > 1:
                order_item.quantity -= 1
                order_item.save()
            else:
                order.orderitems.remove(order_item)
                order_item.delete()
            messages.info(request, f"Количество {item.name} обновленно.")
            return redirect("mainapp:cart-home")
        else:
            messages.info(request, f"Количество {item.name} обновленно.")
            return redirect("mainapp:cart-home")
    else:
        messages.info(request, "У вас нет активного заказа")
        return redirect("mainapp:cart-home")