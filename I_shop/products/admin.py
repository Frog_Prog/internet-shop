from django.contrib import admin

from products.models import Category, Product
from cart.models import Cart, Order

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Cart)
admin.site.register(Order)