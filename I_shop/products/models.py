from django.db import models
from django.urls import reverse

class Category(models.Model):
    class Meta: 
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    title = models.CharField(max_length=300)
    

    def __str__(self):
        """
        Переопределение перевода объекта в строку. Возвращает имя объекта
        """
        return self.title

#Product Model
class  Product(models.Model):


    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    mainimage = models.ImageField(upload_to='products/', blank=True)
    name = models.CharField(max_length=300, verbose_name='Название')
    slug = models.SlugField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    preview_text = models.TextField(max_length=200, verbose_name='Текст превью')
    detail_text = models.TextField(max_length=1000, verbose_name='Подробный текст')
    price = models.FloatField(verbose_name='Цена')
    

    def get_absolute_url(self):
         """
         Возвращает путь к конкретному объекту
         """
         return reverse('mainapp:product-detail', args=[str(self.id)])

    def __str__(self):
        """
        Переопределение перевода объекта в строку. Возвращает имя объекта
        """
        return self.name

    