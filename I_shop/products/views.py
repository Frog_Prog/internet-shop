from unicodedata import category, name
from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView, DetailView
from products.models import Product, Category

class Home(ListView):
    model = Product
    template_name = 'products/home.html'

    def get_queryset(self): #получаем список продуктов
        filter_name = self.request.path
        print(filter_name)
        if filter_name != "/":
            try:
                filter_name=filter_name.replace('/find/','') #если идёт поиск по категории 
                cat = Category.objects.filter(title=filter_name)[0] #получаем категорию по названию
                return Product.objects.filter(category=cat) #фильтруем по ней
            except:
                return Product.objects.all()
        else:
            return Product.objects.all()

class ProductDetailView(DetailView):
    model = Product